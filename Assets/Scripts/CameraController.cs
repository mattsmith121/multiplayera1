﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] float height;
    [SerializeField] Vector3 centerOffset;
    [SerializeField] float smoothSpeed;
    Vector3 cameraOffset;
    Transform followObject;

    public void SetFollowObject(Transform followObj) {
        followObject = followObj;
        SnapTo();
    }

    void LateUpdate() {
        if (followObject != null) {
            Follow();
        }
    }

    void Follow() {
        cameraOffset.z = -distance;
        cameraOffset.y = height;

        transform.position = Vector3.Lerp(transform.position, followObject.position + followObject.TransformVector(cameraOffset), smoothSpeed * Time.deltaTime);

        transform.LookAt(followObject.position + centerOffset);
    }

    void SnapTo() {
        cameraOffset.z = -distance;
        cameraOffset.y = height;


        transform.position = followObject.position + followObject.TransformVector(cameraOffset);


        transform.LookAt(followObject.position + centerOffset);
    }
}
