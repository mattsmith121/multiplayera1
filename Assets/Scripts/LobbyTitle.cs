﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LobbyTitle : MonoBehaviour
{
    [SerializeField] float speed;
    Text text;
    bool r, g, b;
    int v;


    void Start() {
        g = true;
        r = false;
        b = false;
        text = GetComponent<Text>();
        v = 1;
    }

    void FixedUpdate() {
        float s = speed / 255f;
        if (r) {
            if (v == -1 && text.color.r <= 0) {
                r = false;
                g = true;
                Switch();
            }
            else if (v == 1 && text.color.r >= 1) {
                r = false;
                g = true;
                Switch();
            }
            text.color = new Color(text.color.r + s * v, text.color.g, text.color.b);
        } else if (g) {
            if (v == -1 && text.color.g <= 0) {
                g = false;
                b = true;
                Switch();
            } else if (v == 1 && text.color.g >= 1) {
                g = false;
                b = true;
                Switch();
            }
            text.color = new Color(text.color.r, text.color.g + s * v, text.color.b);
        } else if (b) {
            if (v == -1 && text.color.b <= 0) {
                b = false;
                r = true;
                Switch();
            }
            else if (v == 1 && text.color.b >= 1) {
                b = false;
                r = true;
                Switch();
            }
            text.color = new Color(text.color.r, text.color.g, text.color.b + s * v);
        }
    }

    void Switch() {
        if (r) {
            if (text.color.r >= 1) {
                v = -1;
            } else {
                v = 1;
            }
        } else if (g) {
            if (text.color.g >= 1) {
                v = -1;
            }
            else {
                v = 1;
            }
        } else if (b) {
            if (text.color.b >= 1) {
                v = -1;
            }
            else {
                v = 1;
            }
        }
    }
}
