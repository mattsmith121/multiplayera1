﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScreenController : MonoBehaviour
{
    [SerializeField] GameObject errorText;
    string nickName = "";
    public void SetNickname(string name) {
        this.nickName = name;
    }

    public void GoToLobby() {
        if (string.IsNullOrWhiteSpace(PhotonNetwork.NickName)) {
            errorText.SetActive(true);
            return;
        }

        SceneManager.LoadScene(SceneConsts.LobbyScene);
    }
}
