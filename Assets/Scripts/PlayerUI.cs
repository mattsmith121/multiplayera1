﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] Text playerNameText;
    [SerializeField] Vector3 screenOffset = new Vector3(0f, 30f, 0f);
    [SerializeField] float playerHeight;
    Transform targetTransform;
    Renderer targetRenderer;
    CanvasGroup canvasGroup;
    Vector3 targetPosition;
    PlayerController player;

    void Awake() {
        // TODO: change this to just get the canvas from GameManager or something
        transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false);
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void SetPlayer(PlayerController _player) {
        player = _player;

        targetTransform = player.GetComponent<Transform>();
        targetRenderer = player.GetComponent<Renderer>();

        if (playerNameText != null) {
            playerNameText.text = player.photonView.Owner.NickName;
        }
    }

    void Update() {
        if (player == null) {
            Destroy(gameObject);
            return;
        }
    }

    void LateUpdate() {
        // Do not show the UI if we are not visible to the camera, thus avoid potential bugs with seeing the UI, but not the player itself.
        if (targetRenderer != null) {
            canvasGroup.alpha = targetRenderer.isVisible ? 1f : 0f;
        }

        // Follow the Target GameObject on screen.
        if (targetTransform != null) {
            targetPosition = targetTransform.position;
            targetPosition.y += playerHeight;
            transform.position = Camera.main.WorldToScreenPoint(targetPosition) + screenOffset;
        }
    }
}
