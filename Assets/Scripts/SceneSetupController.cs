﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class SceneSetupController : MonoBehaviour
{
    [SerializeField] CameraController mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        Screen.fullScreen = false;
        CreatePlayer();
    }

    private void CreatePlayer() {
            Debug.Log("Creating Player");
            GameObject go = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonPlayer"),
                                        Vector3.zero, Quaternion.identity);
        mainCamera.SetFollowObject(go.transform);
    }

    private void Update() {

    }

}
