﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class LobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField] public InputField roomNameField;
    [SerializeField] public Text errorMessage;
    [SerializeField] Transform rooms;
    [SerializeField] GameObject roomPrefab;
    [SerializeField] Text roomSelectedText;
    string currentRoomSelection;
    List<string> takenNames;

    // Start is called before the first frame update
    void Start()
    {
        takenNames = new List<string>();
        currentRoomSelection = "";
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList) {
        // Destroy all rooms in list
        for (int i = 0; i < rooms.childCount; i++) {
            Destroy(rooms.GetChild(i).gameObject);
        }
        // Clear names
        takenNames.Clear();

        // Create new list
        foreach (RoomInfo room in roomList) {
            if (room.RemovedFromList) {
                continue;
            }
            GameObject r = Instantiate(roomPrefab, rooms);
            r.GetComponent<RoomSelection>().SetSelection(room.Name, this);
            takenNames.Add(room.Name);
        }
    }

    public void SetRoomSelection(string roomName) {
        currentRoomSelection = roomName;
        roomSelectedText.text = roomName;
    }

    public override void OnConnectedToMaster() {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby() {
        base.OnJoinedLobby();
        Debug.Log("Joined lobby!");
    }

    public void JoinRoom() {
        if (string.IsNullOrWhiteSpace(currentRoomSelection)) {
            errorMessage.text = "Please select a room before joining";
            return;
        }
        PhotonNetwork.JoinRoom(currentRoomSelection);
    }

    public void Cancel() {
        PhotonNetwork.LeaveRoom();
    }

    public void CreateRoom() {
        if (string.IsNullOrWhiteSpace(roomNameField.text)) {
            errorMessage.text = "Room Name cannot be empty";
            return;
        } else if (takenNames.Contains(roomNameField.text)) {
            errorMessage.text = "Room Name already taken";
            return;
        }
        else {
            errorMessage.text = "";
        }

        RoomOptions roomOpts = new RoomOptions() {  IsVisible = true,
                                                IsOpen = true, MaxPlayers = 4};

        PhotonNetwork.CreateRoom(roomNameField.text, roomOpts);
    }

    public override void OnCreateRoomFailed(short returnCode, string message) {
        Debug.Log("Failed to create room... trying again.");
        CreateRoom();
    }

    public override void OnJoinedRoom() {
        Debug.Log("Joined Room");
        if (PhotonNetwork.IsMasterClient) {
            Debug.Log("Starting Game");
            PhotonNetwork.LoadLevel(SceneConsts.GameScene);
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message) {
        Debug.Log("Failed to join room... trying again.");
        PhotonNetwork.JoinRoom("roomnamehere");
    }
}
