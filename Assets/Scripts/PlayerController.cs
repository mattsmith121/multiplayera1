﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour
{

    [HideInInspector] public PhotonView photonView;
    [SerializeField] GameObject playerUIPrefab;
    [SerializeField] float speed;
    [SerializeField] float rotateSpeed;
    [SerializeField] Transform projectileSpawn;
    [SerializeField] float health;
    [SerializeField] AudioClip fireSound;
    [SerializeField] AudioClip hitSound;
    [SerializeField] MeshRenderer meshRenderer;
    AudioSource audio;
    

    void Start() {
        audio = GetComponent<AudioSource>();
        photonView = GetComponent<PhotonView>();
        health = 100;
        if (playerUIPrefab != null) {
            GameObject ui = Instantiate(playerUIPrefab);
            ui.GetComponent<PlayerUI>().SetPlayer(this);
        }
    }


    void Update()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true) {
            return;
        }

        MovePlayer();
    }

    void MovePlayer() {
        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.position += -transform.right * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.position += transform.right * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.UpArrow)) {
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.DownArrow)) {
            transform.position += -transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.Q)) {
            transform.Rotate(0, -rotateSpeed * Time.deltaTime, 0f);
        }

        if (Input.GetKey(KeyCode.E)) {
            transform.Rotate(0, rotateSpeed * Time.deltaTime, 0f);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Projectile"), 
                projectileSpawn.position, transform.rotation);
            photonView.RPC("PlayFireSound", RpcTarget.All);
        }
    }

    [PunRPC]
    public void PlayFireSound() {
        audio.PlayOneShot(fireSound);
    }

    public void Hit() {
        if (photonView.IsMine) {
            health -= 50;
            if (health <= 0) {
                StartCoroutine(DelayDisconnect());
            }
        }
        audio.PlayOneShot(hitSound);
    }

    IEnumerator DelayDisconnect() {
        photonView.RPC("ChangeColor", RpcTarget.All);
        yield return new WaitForSeconds(1f);
        Disconnect();
    }

    [PunRPC]
    public void ChangeColor() {
        if (meshRenderer != null) {
            meshRenderer.material.color = Color.black;
        }
    }

    void Disconnect() {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(SceneConsts.LobbyScene);
    }
}
