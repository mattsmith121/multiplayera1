﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomSelection : MonoBehaviour
{
    [SerializeField] Text roomText;
    string roomName;
    LobbyController lobbyController;

    public void SetSelection(string roomName, LobbyController lobbyController) {
        this.roomName = roomName;
        this.lobbyController = lobbyController;
        roomText.text = roomName;
    }

    public void RoomSelected() {
        lobbyController.SetRoomSelection(roomName);
    }

}
