﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class Projectile : MonoBehaviour
{
    [SerializeField] float speed;
    bool off;
    PhotonView photonView;

    void Start() {
        off = false;
        photonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    void OnCollisionEnter(Collision collision) {


        // Only collide once
        if (off) {
            return;
        }

        if (collision.gameObject.tag != "Player") {
            // Once projectile hits wall or something its done
            DestroyProjectile();
        }

        if (collision.gameObject.tag == "Player") {
            // Damage player
            collision.gameObject.GetComponent<PlayerController>().Hit();
            // disable future collisions
            off = true;
            // Destroy after delay
            StartCoroutine(DelayedDestroy());
        }
    }

    IEnumerator DelayedDestroy() {
        yield return new WaitForSeconds(0.2f);
        DestroyProjectile();
    }

    void DestroyProjectile() {
        // If not my projectile, ignore
        if (!photonView.IsMine) {
            return;
        }
        PhotonNetwork.Destroy(GetComponent<PhotonView>());
    }
}
